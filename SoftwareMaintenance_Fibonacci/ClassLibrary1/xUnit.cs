﻿using Xunit;
using System.Text;
using System.Diagnostics;
using System.IO;

namespace ClassLibrary1
{
    public class xUnit
    {
        public void runPythonScript(int lenght, int start)
        {
            string command = "python C:/Users/arttu/source/repos/SoftwareMaintenance_Weekly/SoftwareMaintenance_Fibonacci/ClassLibrary1/SoftwareMaintenance_Fibonacci.py " + lenght.ToString() + " " + start.ToString();

            Process p = new Process();
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "cmd.exe";
            info.RedirectStandardInput = true;
            info.UseShellExecute = false;

            p.StartInfo = info;
            p.Start();

            using (StreamWriter sw = p.StandardInput)
            {
                if (sw.BaseStream.CanWrite)
                {
                    sw.WriteLine("cd c:/Python27/");
                    sw.WriteLine(command);
                }
            }
            p.Close();
        }

        [Fact]
        public void Outputs()
        {
            int lenght = 10;
            int start = 5;

            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            string[] lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Equal(lenght, lines.Length);
            Assert.Equal("8", lines[0]);
            Assert.Equal("13", lines[1]);
            Assert.Equal("21", lines[2]);
            Assert.Equal("34", lines[3]);
            Assert.Equal("55", lines[4]);
            Assert.Equal("89", lines[5]);
            Assert.Equal("144", lines[6]);
            Assert.Equal("233", lines[7]);
            Assert.Equal("377", lines[8]);
            Assert.Equal("610", lines[9]);

            lenght = 4;
            start = 3;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Equal(lenght, lines.Length);
            Assert.Equal("3", lines[0]);
            Assert.Equal("5", lines[1]);
            Assert.Equal("8", lines[2]);
            Assert.Equal("13", lines[3]);
        }

        [Fact]
        public void LinesAmount()
        {
            int lenght;
            int start;
            string[] lines;

            lenght = 123;
            start = 33;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Equal(lenght, lines.Length);

            lenght = 5555;
            start = 22;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Equal(lenght, lines.Length);

            lenght = 3;
            start = 3;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Equal(lenght, lines.Length);
        }

        [Fact]
        public void FaultyInputs()
        {
            int lenght;
            int start;
            string[] lines;

            lenght = 0;
            start = 33;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Empty(lines);

            lenght = 1;
            start = 0;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Empty(lines);

            lenght = 5;
            start = -5;
            runPythonScript(lenght, start);
            System.Threading.Thread.Sleep(500);
            lines = File.ReadAllLines(@"C:\Users\arttu\source\repos\SoftwareMaintenance_Weekly\SoftwareMaintenance_Fibonacci\ClassLibrary1\output.txt", Encoding.UTF8);
            Assert.Empty(lines);
        }
    }
}


