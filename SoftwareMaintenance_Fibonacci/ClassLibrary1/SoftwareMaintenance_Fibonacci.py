#!python
import sys


def main():
    # clear the file contents
    open('C:/Users/arttu/source/repos/SoftwareMaintenance_Weekly/SoftwareMaintenance_Fibonacci/ClassLibrary1/output.txt', mode='w').close()

    ## Use this if you want to insert arguments when running the script
    #while (True):
    #    try:
    #        seriesLenght = int(input("Lenght: "))
    #        break
    #    except:
    #        print("Please insert integer!")
    #while (True):
    #    try:
    #        startingNo = int(input("Starting index: "))
    #        if (startingNo >= 3):
    #            break
    #        else:
    #            print("Index too small.")
    #    except:
    #        print("Please insert integer!")

    ## Use this if you want to insert arguments as cmd args (Works with tests)
    seriesLenght = int(sys.argv[1])
    startingNo = int(sys.argv[2])

    while (True):
        try:
            seriesLenght = int(sys.argv[1])
            startingNo = int(sys.argv[2])
            if (startingNo >= 3 and seriesLenght >= startingNo):
                break
        except:
            print("Invalid arguments given. Only integers accepted, where 1:st arg > 2:nd arg and 2:nd arg > 2")

    prevFirst = 0
    prevSecond = 0

    # 1st value of sequence
    value = 1

    # Init the sequence to input point
    for i in range(1, startingNo):
        prevFirst = prevSecond
        prevSecond = value
        value = prevFirst + prevSecond

    # At this point, value = Fibonacci at startingNo
    try:
        file = open('C:/Users/arttu/source/repos/SoftwareMaintenance_Weekly/SoftwareMaintenance_Fibonacci/ClassLibrary1/output.txt', mode='a')
        try:
            for i in range(0, seriesLenght):
                prevFirst = prevSecond
                prevSecond = value
                value = prevFirst + prevSecond
                try:
                    file.write(str(value) + '\n')
                except:
                    print("Writing to file failed!")
            file.close()
        except:
            print("An error occured!")
    except:
        print("couldn't open file!")

main()
